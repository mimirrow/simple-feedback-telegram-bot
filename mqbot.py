from telegram.ext import messagequeue as mq
import telegram.bot
from telegram.utils.request import Request
import functools


def auto_group(method):
    @functools.wraps(method)
    def wrapped(self, *args, **kwargs):
        return method(self, *args, **kwargs, isgroup=True)


class MQBot(telegram.bot.Bot):
    def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
        super(MQBot, self).__init__(*args, **kwargs)
        self._is_messages_queued_default = True
        self._msg_queue = mqueue or mq.MessageQueue()

    def __del__(self):
        try:
            self._msg_queue.stop()
        except:
            pass

    @mq.queuedmessage
    def send_message(self, *args, **kwargs):
        return super(MQBot, self).send_message(*args, **kwargs, is_group=True)

    @mq.queuedmessage
    def forward_message(self, *args, **kwargs):
        return super(MQBot, self).forward_message(*args, **kwargs, is_group=True)

    @mq.queuedmessage
    def send_voice(self, *args, **kwargs):
        return super(MQBot, self).send_voice(*args, **kwargs)

    @mq.queuedmessage
    def send_video(self, *args, **kwargs):
        return super(MQBot, self).send_video(*args, **kwargs)

    @mq.queuedmessage
    def send_sticker(self, *args, **kwargs):
        return super(MQBot, self).send_sticker(*args, **kwargs)

    @mq.queuedmessage
    def send_animation(self, *args, **kwargs):
        return super(MQBot, self).send_animation(*args, **kwargs)

    @mq.queuedmessage
    def send_document(self, *args, **kwargs):
        return super(MQBot, self).send_document(*args, **kwargs)

    @mq.queuedmessage
    def send_audio(self, *args, **kwargs):
        return super(MQBot, self).send_audio(*args, **kwargs)

    @mq.queuedmessage
    def send_photo(self, *args, **kwargs):
        return super(MQBot, self).send_photo(*args, **kwargs)


class MHBot:
    def __init__(self, token_key):
        self.q = mq.MessageQueue(group_burst_limit=2, group_time_limit_ms=7000)
        self.request = Request(con_pool_size=8)
        self.MQBot = MQBot(token=token_key, request=self.request, mqueue=self.q)
