# encoding=utf8

import json
from telegram.ext import Updater
from telegram.ext import CommandHandler, MessageHandler, Filters
from mqbot import MHBot

import logging
logging.basicConfig(filename='app.log', filemode='a', format='%(name)s - %(levelname)s - %(message)s')
logging.warning('This will get logged to a file')


class UserInteraction:
    def __init__(self):
        self.config = json.load(open('config.json', encoding='utf-8'))
        self.token_key = self.config['token_key']
        self.message_queue_bot = MHBot(self.token_key)
        self.updater = Updater(bot=self.message_queue_bot.MQBot, use_context=True)
        self.dispatcher = self.updater.dispatcher
        self.came_from = 'Пришло от'
        self.add_handlers()
        self.start_polling()

    def update_config(self, **kwargs):
        with open('config.json', 'w') as f:
            if 'ban' in kwargs:
                for x in kwargs['ban']:
                    self.config['banned'][str(x)] = True
            if 'unban' in kwargs:
                for x in kwargs['unban']:
                    self.config['banned'].pop(str(x), None)
            json.dump(self.config, f)
            return

    def add_handlers(self):
        self.dispatcher.add_handler(CommandHandler('ban', self.command_ban))
        self.dispatcher.add_handler(CommandHandler('unban', self.command_ban))
        self.dispatcher.add_handler(CommandHandler('blacklist', self.command_blacklist))
        self.dispatcher.add_handler(MessageHandler(Filters.all, self.handle_message))

    def is_admin_chat(self, update):
        message = update.message
        return int(message.chat.id) == int(self.config['admin_chat'])

    def command_ban(self, update, context):
        ban = []
        unban = []
        users = []
        if not self.is_admin_chat(update):
            return
        if len(context.args) == 0:
            user_id = int(update.message.reply_to_message.text.split('|')[-1])
            users = [user_id]
        else:
            users = list(map(int, context.args))
        if update.message.text.split(' ')[0] == '/ban':
            ban = users
        else:
            unban = users
        self.update_config(ban=ban, unban=unban)

    def command_blacklist(self, update, context):
        if not self.is_admin_chat(update):
            return
        if int(update.message.chat.id) == int(self.config['admin_chat']):
            text = 'Список забаненых:\n' + '\n'.join(map(str, self.config['banned']))
            context.bot.send_message(chat_id=self.config['admin_chat'], text=text)

    def start_polling(self):
        self.updater.start_polling()
        self.updater.idle()

    def handle_message(self, update, context):
        message = update.message
        if int(message.chat.id) == int(self.config['admin_chat']):
            self.admin_message(update, context)
        else:
            self.user_message(update, context)

    def user_message(self, update, context):
        try:
            in_message = update.message
            message_id = in_message.message_id
            from_id = in_message.chat.id

            # check on private message
            if int(from_id) < 0 or from_id in self.config['banned']:
                return

            # fixed messages: anti spam/hidden features
            if in_message.text == '/start':
                context.bot.send_message(chat_id=self.config['admin_chat'],
                                         text=f'{in_message.from_user.name}|{in_message.from_user.id} пишет в чат')
                return

            # send info about user and forward message
            context.bot.send_message(chat_id=self.config['admin_chat'],
                                     text=f'{self.came_from}: {in_message.from_user.name}|{in_message.from_user.id}')
            context.bot.forward_message(self.config['admin_chat'], from_id, message_id)
        except Exception as e:
            logging.error('User handle error')
            logging.error(str(e))

    def admin_message(self, update, context):
        try:
            message = update.message
            message_id = message.message_id
            if message.reply_to_message is None:
                return
            if message.reply_to_message.from_user.username != self.config['bot_name']:
                return
            text = message.reply_to_message.text
            if text.split(':')[0] == self.came_from:
                user_id = int(text.split('|')[-1])
                # ban from sending something again
                if message.text == '/ban':
                    self.update_config(ban=[user_id])
                    return
                if message.text == '/unban':
                    self.update_config(unban=[user_id])
                    return
                # normal reply
                if message.photo != []:
                    context.bot.send_photo(chat_id=user_id, photo=message.photo[0], caption=message.caption)
                if message.audio is not None:
                    context.bot.send_audio(chat_id=user_id, audio=message.audio)
                if message.document is not None and message.animation is None:
                    context.bot.send_document(chat_id=user_id, document=message.document)
                if message.animation is not None:
                    context.bot.send_animation(chat_id=user_id, animation=message.animation)
                if message.sticker is not None:
                    context.bot.send_sticker(chat_id=user_id, sticker=message.sticker)
                if message.video is not None:
                    context.bot.send_video(chat_id=user_id, video=message.video)
                if message.voice is not None:
                    context.bot.send_voice(chat_id=user_id, voice=message.voice)
                if message.text is not None:
                    context.bot.send_message(chat_id=user_id, text=message.text)
        except Exception as e:
            logging.error('Admin handle error')
            logging.error(str(e))


UserInteraction()

